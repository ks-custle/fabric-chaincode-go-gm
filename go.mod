module gitee.com/ks-custle/fabric-chaincode-go-gm

go 1.17

require (
	gitee.com/ks-custle/core-gm v0.0.0-20230922171213-b83bdd97b62c
	gitee.com/ks-custle/fabric-protos-go-gm v0.0.0-20230922172715-d6d075372b7e
	github.com/golang/protobuf v1.5.2
	github.com/stretchr/testify v1.7.1-0.20210116013205-6990a05d54c2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.1.0 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20211208223120-3a66f561d7aa // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

//replace (
// 	gitee.com/ks-custle/fabric-protos-go-gm => /home/go-workspace/src/github.com/hyperledger/fabric-protos-go-gm
// 	gitee.com/ks-custle/core-gm => /home/go-workspace/src/github.com/hyperledger/core-gm
//)

// replace github.com/hyperledger/fabric-protos-go => github.com/hyperledger/fabric-protos-go v0.0.0-20200506201313-25f6564b9ac4
